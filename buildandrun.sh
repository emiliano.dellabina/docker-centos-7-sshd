#!/bin/bash
docker build -t docker-centos-7-sshd .
docker run -dt --privileged=true --name galera-node --restart=always -v /data/data-cluster:/var/lib/mysql --network host docker-centos-7-sshd
