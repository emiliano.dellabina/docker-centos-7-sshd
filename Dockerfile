FROM centos:7
MAINTAINER Emiliano Della Bina  <emiliano.dellabina@ingv.it>

RUN yum -y install openssh-server openssh-clients epel-release sudo && \
    rm -f /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_dsa_key && \
    ssh-keygen -q -N "" -t rsa -f /root/.ssh/id_rsa

RUN yum -y install pwgen

COPY sshd_config /etc/ssh/sshd_config
COPY install_sshd.sh /install_sshd.sh

EXPOSE 2323 3306
RUN systemctl enable sshd

## use as a base and then add these 
RUN /install_sshd.sh
RUN echo 'root:toor1234' | chpasswd
CMD ["/usr/sbin/init"]
